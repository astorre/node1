defmodule Node1.Poller do
  @moduledoc """
  Long-polling process. Consumes Telegram channel messages
  and send them to rabbit queue.
  """

  require Logger

  use GenServer
  use AMQP

  @mq_url Application.get_env(:amqp, :mq_url)
  @exchange "node2_exchange"

  defstruct chan: %{}, offset: 0

  @type t :: %Node1.Poller{}

  # API

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def update do
    GenServer.cast(__MODULE__, :update)
  end

  # Server

  def init(:ok) do
    conn = try_connect()
    {:ok, chan} = Channel.open(conn)
    update()
    {:ok, %__MODULE__{chan: chan, offset: 0}}
  end

  @spec handle_cast(atom(), Node1.Poller.t()) :: {:noreply, Node1.Poller.t()}
  def handle_cast(:update, poller) do
    new_offset =
      Nadia.get_updates(offset: poller.offset)
      |> process_messages(poller.chan)

    {:noreply, %{poller | offset: new_offset + 1}, 100}
  end

  def handle_info(:timeout, offset) do
    update()
    {:noreply, offset}
  end

  defp try_connect do
    case Connection.open(@mq_url) do
      {:ok, conn} ->
        conn

      {:error, reason} ->
        Logger.log(:error, "failed for #{inspect(reason)}")
        :timer.sleep(5000)
        try_connect()
    end
  end

  defp process_messages({:ok, []}, _chan), do: -1

  @spec process_messages({:ok, list()}, Node1.Poller.t()) :: integer()
  defp process_messages({:ok, results}, chan) do
    telegram_channel_id = Application.get_env(:nadia, :telegram_channel_id)

    last_id =
      results
      |> Enum.filter(fn message ->
        Integer.to_string(message_struct(message).chat.id) == telegram_channel_id
      end)
      |> Enum.map(fn %{update_id: id} = message ->
        message
        |> send_to_mq_channel(chan)

        id
      end)
      |> List.last()

    case last_id do
      nil ->
        -1

      _ ->
        last_id
    end
  end

  defp process_messages({:error, %Nadia.Model.Error{reason: reason}}, _chan) do
    Logger.log(:error, reason)
    -1
  end

  defp process_messages({:error, error}, _chan) do
    Logger.log(:error, error)
    -1
  end

  defp send_to_mq_channel(nil, _chan), do: Logger.log(:info, "nil")

  defp send_to_mq_channel(message, chan) do
    message_s = message_struct(message)

    case message_s.photo do
      [] ->
        text = message_s.text
        Logger.log(:info, text)
        Basic.publish(chan, @exchange, "", text)

      photo ->
        get_file_paths(photo, chan)
    end
  end

  defp get_file_paths(photo_sizes, chan) do
    photo_sizes
    |> Enum.map(fn ps ->
      Task.async(fn ->
        get_file_path_from(ps.file_id, chan)
      end)
    end)
  end

  defp get_file_path_from(file_id, chan) do
    response = prepare_request(file_id)

    case response do
      {:ok, body} ->
        body
        |> parse_response
        |> handle_response(chan)

      {:error, reason} ->
        Logger.log(:error, reason)

      _ ->
        Logger.log(:error, "Unexpected :httpc.request response!")
    end
  end

  defp prepare_request(file_id) do
    "https://api.telegram.org/bot#{Application.get_env(:nadia, :token)}/getFile?file_id=#{
      file_id
    }"
    |> to_charlist
    |> request
  end

  defp parse_response(raw_response) do
    raw_response
    |> to_string
    |> Poison.decode!()
  end

  defp handle_response(%{"ok" => true, "result" => %{"file_id" => _file_id, "file_size" => _file_size, "file_path" => file_path}}, chan) do
    file_url =
      "https://api.telegram.org/file/bot#{Application.get_env(:nadia, :token)}/#{
        file_path
      }"

    Basic.publish(chan, @exchange, "", file_url)
  end
  defp handle_response(%{"ok" => false, "error_code" => _error_code, "description" => description}, _chan) do
    Logger.log(:error, description)
  end
  defp handle_response(response, _chan) do
    Logger.log(:error, inspect(response))
  end

  defp request(file_uri) do
    Application.ensure_all_started(:inets)

    Application.ensure_all_started(:ssl)

    case :httpc.request(:get, {file_uri, []}, [{:timeout, :timer.seconds(5)}], []) do
      {:ok, {{'HTTP/1.1', 200, 'OK'}, _headers, body}} ->
        {:ok, body}

      {:error, resp} ->
        case resp do
          :timeout ->
            {:error, "ERROR Response: timeout"}

          resp_tuple ->
            {:error, "ERROR Response: #{resp_tuple |> Tuple.to_list() |> hd}"}
        end
    end
  end

  defp message_struct(message) do
    case message.channel_post do
      nil ->
        message.message

      post ->
        post
    end
  end
end
